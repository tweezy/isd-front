$(function() {
	new WOW().init();
	$(window).scroll(function() {
    	if ($(this).scrollTop() > 100) {
     		$('#header').addClass('header-down');
    	} else {
     		$('#header').removeClass('header-down');
    	}
  	});	

	if ($(window).scrollTop() > 100) {
		$('#header').addClass('header-down');
	} 
	$('.mobile-nav-btn').click(function(event) {
		if ($('.mobile-nav-btn').hasClass('mobile-btn-has-overlay')) {
			$('body').removeClass('overlay')
			$('.mobile-nav-btn').removeClass('mobile-btn-has-overlay').html('<i class="fa fa-bars"></i>').css('color','#1B4B7C')
			$('.mobile-nav').css('left', -260)
			$('.mobile-nav-overlay').css('display','none')
		} else {
			$('.mobile-nav-overlay').css('display','block')
			$('.mobile-nav').css('left', 0)
			$('body').addClass('overlay')
			$('.mobile-nav-btn').addClass('mobile-btn-has-overlay').html('<i class="fa fa-times"></i>').css('color', 'white')
		}
	});
	$('.mobile-nav-links a').click(function(event) {
		$('body').removeClass('overlay')
		$('.mobile-nav-btn').removeClass('mobile-btn-has-overlay').html('<i class="fa fa-bars"></i>').css('color','#1B4B7C')
		$('.mobile-nav').css('left', -260)
		$('.mobile-nav-overlay').css('display','none')
	});

	var owl = $('.intro-carousel');
	owl.owlCarousel({
      	dots: false,
	    loop: true,
	    margin:200,
      	autoplay: true,
      	autoplaySpeed: 1500,
      	items: 1,
      	autoplayHoverPause: true
	});
	$('.play').on('click',function(){
	    owl.trigger('play.owl.autoplay',[1000])
	})
	$('.stop').on('click',function(){
	    owl.trigger('stop.owl.autoplay')
	})

	var owl2 = $('.testimonials-carousel');
	owl2.owlCarousel({
      	autoplay: true,
	    dots: true,
	    loop: true,
	    items: 1,
	    center: true,
	    autoplayHoverPause: true
	});
})

$(document).ready(function(){
	$(".main-nav a, .mobile-nav a, .custom-btn").on('click', function(event) {
	    if (this.hash !== "") {
	      	event.preventDefault();
	      	var hash = this.hash;
	      	$('html, body').animate({
	        	scrollTop: $(hash).offset().top
	      	}, 1000, function(){
	        	window.location.hash = hash;
	      	});
	    } 
	});
});

var nav_sections = $('section');
var main_nav = $('.main-nav, .mobile-nav');
var main_nav_height = $('#header').outerHeight();
$(window).on('scroll', function () {
	var cur_pos = $(this).scrollTop();
		nav_sections.each(function() {
  		var top = $(this).offset().top - main_nav_height,
      	bottom = top + $(this).outerHeight();
	  	if (cur_pos >= top && cur_pos <= bottom) {
	    	main_nav.find('li').removeClass('active');
	    	main_nav.find('a[href="#'+$(this).attr('id')+'"]').parent('li').addClass('active');
	  	}
	});
});